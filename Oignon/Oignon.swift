//
//  Oignon.swift
//  Oignon
//
//  Created by Erwan Hesry on 02/07/2020.
//  Copyright © 2020 Erwan Hesry. All rights reserved.
//

import Foundation

public protocol Ring {
    var position:Oignon.RingPosition { get set }
    var messagesAvailable: [String] {get set}
    var seed: Oignon? { get set }
    
    mutating func bloomed()
    func useMessage(_ message:String, _ data:Any?, end: @escaping (_ dataFromRings:Any?, _ errorFromRings:Error?) -> Void)
}

public class Oignon {
    var rings: [Ring] = []
    
    public init() {}
    
    public func seed() {
        rings.forEach { (ring) in
            var ring = ring
            ring.bloomed()
        }
        rings = []
    }
    
    public func growsWithRing(_ ring: inout Ring) {
        ring.seed = self
        rings.append(ring)
        self.sortRings()
        self.displayOignon()
    }
    
    public func bloomRing(_ forMessages:[String]) {
        rings = rings.filter { (ringToTest) -> Bool in
            if ringToTest.messagesAvailable.containsSameElements(as: forMessages) {
                var ringToTest = ringToTest
                ringToTest.bloomed()
                return false
            } else {
                return true
            }
        }
        self.sortRings()
        self.displayOignon()
    }
    
    public func grow(_ messageToRings:String, _ dataToRings:Any?, endOfGerm: @escaping (_ dataFromRings:Any?, _ errorFromRings:Error?) -> Void) {
        let messages = messageToRings.split(separator: ".").map(String.init)
        if let message = messages.first, let ringForMessage = self.getRing(message) {
            self.useRing(ringForMessage, messages, message, dataToRings, nil, endOfGerm)
        }
    }
}

extension Oignon {
    private func sortRings() {
        rings.sort { (aRing, anotherRing) -> Bool in
            return aRing.position < anotherRing.position
        }
    }
    
    private func getRing(_ forMessage:String) -> Ring? {
        return rings.filter { (ringToTest) -> Bool in
            return ringToTest.messagesAvailable.contains(forMessage)
        }.first
    }
    
    private func useRing(_ ring:Ring, _ messages:[String], _ message:String, _ data:Any?, _ error:Error?, _ end:  @escaping (_ dataFromRings:Any?, _ errorFromRings:Error?) -> Void) {
        if let currentMessageIndex = messages.firstIndex(of: message) {
            let nextMessageIndex = currentMessageIndex + 1
            ring.useMessage(message, data) { (ringData, ringError) in
                if nextMessageIndex < messages.count {
                    let nextMessage = messages[nextMessageIndex]
                    if let ringForMessage = self.getRing(nextMessage) {
                        self.useRing(ringForMessage, messages, nextMessage, ringData, ringError, end)
                    } else {
                        end(ringData, ringError)
                    }
                } else {
                    end(ringData, ringError)
                }
            }
        } else {
            end(data, error)
        }
    }
    
    private func displayOignon() {
        print("Here is your Oignon")
        var oignon = "•"
        rings.forEach { (ring) in
            oignon.append(String.init(repeating: " ", count: ring.messagesAvailable.count))
            oignon.append("|")
        }
        print(oignon)
    }
}

extension Oignon {
    public enum RingPosition {
        case Exterior
        case Interior
        case Germ
        
        private var sortOrder: Int {
            switch self {
                case .Exterior:
                    return 2
                case .Interior:
                    return 1
                case .Germ:
                    return 0
            }
        }

         static public func ==(lhs: RingPosition, rhs: RingPosition) -> Bool {
            return lhs.sortOrder == rhs.sortOrder
        }

        static public func <(lhs: RingPosition, rhs: RingPosition) -> Bool {
           return lhs.sortOrder < rhs.sortOrder
        }
    }
}
