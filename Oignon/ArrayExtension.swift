//
//  ArrayExtension.swift
//  Oignon
//
//  Created by Erwan Hesry on 08/07/2020.
//  Copyright © 2020 Erwan Hesry. All rights reserved.
//

import Foundation

extension Array where Element: Comparable {
    func containsSameElements(as other: [Element]) -> Bool {
        return self.count == other.count && self.sorted() == other.sorted()
    }
}
